package com.timofeev.dmdgraphapp;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.timofeev.dmdgraphapp.di.Injector;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application{

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        setupService();
    }

    private void setupService() {

        Realm.init(this);
        Injector.init(this);
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
    }

    public static Context getContext() {
        return sContext;
    }
}

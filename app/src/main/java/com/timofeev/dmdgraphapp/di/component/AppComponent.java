package com.timofeev.dmdgraphapp.di.component;

import android.content.Context;

import com.timofeev.dmdgraphapp.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();

    DataManagerComponent.Builder dataComponent();
}

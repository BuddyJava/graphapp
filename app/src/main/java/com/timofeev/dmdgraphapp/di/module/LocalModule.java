package com.timofeev.dmdgraphapp.di.module;


import com.timofeev.dmdgraphapp.data.managers.RealmService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalModule {

    @Provides
    @Singleton
    RealmService provideRealmManger(){
        return new RealmService();
    }
}

package com.timofeev.dmdgraphapp.di.module;

import com.timofeev.dmdgraphapp.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {

    @Singleton
    @Provides
    DataManager provideDataManager(){
        return DataManager.getInstance();
    }
}

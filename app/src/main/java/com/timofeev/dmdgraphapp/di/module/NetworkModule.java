package com.timofeev.dmdgraphapp.di.module;

import com.timofeev.dmdgraphapp.data.network.ServiceGenerator;
import com.timofeev.dmdgraphapp.data.network.servicies.CbService;
import com.timofeev.dmdgraphapp.data.network.servicies.ForexService;
import com.timofeev.dmdgraphapp.utils.ApiConfig;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


@Module
public class NetworkModule {

//    @Provides
//    @Singleton
//    OkHttpClient provideOkHttpClient() {
//        return createClient();
//    }
//
//    @Provides
//    @Singleton
//    Retrofit provideRetrofit(OkHttpClient client) {
//        return createRetrofit(client);
//    }

    @Provides
    @Singleton
    CbService provideCbService() {
        return new ServiceGenerator().createService(ApiConfig.BASE_URL_CB,ServiceGenerator.XML).create(CbService.class);
//        return retrofit.create(CbService.class);
    }

    @Provides
    @Singleton
    ForexService provideForexService() {
        return new ServiceGenerator().createService(ApiConfig.BASE_URL_FOREX, ServiceGenerator.JSON).create(ForexService.class);
    }

//    private Retrofit createRetrofit(OkHttpClient client) {
//        return new Retrofit.Builder()
//                .baseUrl(ApiConfig.BASE_URL_CB)
//                .addConverterFactory(SimpleXmlConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .client(client)
//                .build();
//    }
//
//    private OkHttpClient createClient() {
//        return new OkHttpClient.Builder()
//                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//                .connectTimeout(ApiConfig.MAX_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
//                .readTimeout(ApiConfig.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS)
//                .writeTimeout(ApiConfig.MAX_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
//                .build();
//    }

}

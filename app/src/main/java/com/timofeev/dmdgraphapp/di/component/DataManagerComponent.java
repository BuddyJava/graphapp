package com.timofeev.dmdgraphapp.di.component;

import com.timofeev.dmdgraphapp.data.managers.DataManager;
import com.timofeev.dmdgraphapp.di.module.LocalModule;
import com.timofeev.dmdgraphapp.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {NetworkModule.class, LocalModule.class})
public interface DataManagerComponent {

    @Subcomponent.Builder
    interface Builder {
        DataManagerComponent.Builder networkModule(NetworkModule network);

        DataManagerComponent build();
    }

    void inject(DataManager dataManager);

}

package com.timofeev.dmdgraphapp.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.timofeev.dmdgraphapp.di.component.AppComponent;
import com.timofeev.dmdgraphapp.di.component.DaggerAppComponent;
import com.timofeev.dmdgraphapp.di.module.AppModule;

public class Injector {

    private static AppComponent appComponent;

    public static void init(@NonNull Context context) {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(context))
                .build();
    }

    @NonNull
    public static AppComponent getAppComponent() {
        if (appComponent == null) {
            throw new RuntimeException("AppComponent is NULL!");
        }
        return appComponent;
    }
}

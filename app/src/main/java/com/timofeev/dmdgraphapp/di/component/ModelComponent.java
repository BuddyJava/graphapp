package com.timofeev.dmdgraphapp.di.component;


import com.timofeev.dmdgraphapp.core.BaseModel;
import com.timofeev.dmdgraphapp.di.module.ModelModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = ModelModule.class)
public interface ModelComponent {

    void inject(BaseModel model);
}

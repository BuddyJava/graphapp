package com.timofeev.dmdgraphapp.features.root;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.timofeev.dmdgraphapp.core.BaseModel;
import com.timofeev.dmdgraphapp.data.network.res.cb.CbRes;
import com.timofeev.dmdgraphapp.data.network.res.forex.ForexRes;
import com.timofeev.dmdgraphapp.data.storage.CbRealm;
import com.timofeev.dmdgraphapp.data.storage.ForexRealm;
import com.timofeev.dmdgraphapp.data.storage.LastUpdateRealm;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import rx.Observable;

public class RootModel extends BaseModel {


    @RxLogObservable
    public Observable updateCursVal(Date date) {
        return Observable.concat(fromNetworkCb(date), fromNetworkForex(date))
                .doOnCompleted(() -> saveUpdateVal(date))
                .timeout(5, TimeUnit.SECONDS);
    }

    private Observable<ForexRes> fromNetworkForex(Date date) {
        return mDataManager.getForexCursFromNetwork(date);
    }

    private Observable<CbRes> fromNetworkCb(Date date) {
        return mDataManager.getCbResFromNetwork(date);
    }

    private void saveUpdateVal(Date date){
        mDataManager.saveLastUpdate(date);
    }

    //    @RxLogObservable
    public Observable<CbRealm> fromDiskCb() {
        return mDataManager.getCbRealmFromRealm().takeLast(7);
    }

    //    @RxLogObservable
    public Observable<ForexRealm> fromDiskForex() {
        return mDataManager.getForexRealmFromRealm().takeLast(7);
    }


    public Observable<LastUpdateRealm> getLastUpdate() {
        return mDataManager.getLastUpdateFromRealm();
    }
}

package com.timofeev.dmdgraphapp.features.root;

import com.timofeev.dmdgraphapp.core.IPresenter;

public interface IRootPresenter extends IPresenter{

    void initView();
    void onClickUpdate();
}

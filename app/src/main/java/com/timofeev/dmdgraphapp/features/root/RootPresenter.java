package com.timofeev.dmdgraphapp.features.root;

import android.support.annotation.NonNull;


import com.jjoe64.graphview.series.DataPoint;
import com.timofeev.dmdgraphapp.core.BasePresenter;
import com.timofeev.dmdgraphapp.core.IView;
import com.timofeev.dmdgraphapp.utils.DateHelper;

import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;


public class RootPresenter extends BasePresenter<GraphActivity> implements IRootPresenter {


    public static RootPresenter INSTANCE = null;

    private RootModel mRootModel = new RootModel();
    private CompositeSubscription mComSub = new CompositeSubscription();


    private RootPresenter() {
    }

    public static RootPresenter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RootPresenter();
        }
        return INSTANCE;
    }

    @Override
    public void bindView(@NonNull IView view) {
        super.bindView(view);
    }

    @Override
    public void unbindView() {

        if (mComSub.hasSubscriptions()) {
            mComSub.unsubscribe();
        }
        super.unbindView();
    }

    //region ============= IRootPresenter ===============


    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProgress();
            mComSub.add(fromDiskValCurs());
            mComSub.add(subscribeOnUpdateFromRealm());
        }
    }

    @Override
    public void onClickUpdate() {

        if (getView() != null) {
            getView().showProgress();
        }
        Date date = new Date();
        mComSub.add(subscribeOnUpdateCurs(date));
    }


    //endregion

    //region ============= SUBSCRIPTION ===============

    private Subscription subscribeOnUpdateCurs(Date date) {

        return mRootModel.updateCursVal(date)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber() {
                    @Override
                    public void onCompleted() {
                        if (getView() != null) {
                            mComSub.add(fromDiskValCurs());
                            mComSub.add(subscribeOnUpdateFromRealm());
                            getView().hideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (getView() != null) {
                            getView().showMessage(e.getMessage());
                            getView().hideProgress();
                        }
                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });


    }

    private Subscription fromDiskValCurs() {
        return Observable.concat(subscribeOnCbFromRealmObs(), subscribeOnForexFromRealmObs())
                .subscribe(new Subscriber<List<DataPoint>>() {
                    @Override
                    public void onCompleted() {
                        if (getView() != null) {
                            getView().hideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (getView() != null) {
                            getView().hideProgress();
                            getView().showMessage(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(List<DataPoint> dataPoints) {

                    }
                });

    }

    private Subscription subscribeOnUpdateFromRealm() {
        return mRootModel.getLastUpdate().subscribe(
                lastUpdateRealm -> {
                    if (getView() != null) {
                        getView().showLastUpdate(DateHelper.formatDateForTextView(lastUpdateRealm.getLastUpdate()));
                    }
                }, throwable -> {
                    if (getView() != null) {
                        getView().showMessage(throwable.getMessage());
                    }
                });
    }


//    private Observable<List<Entry>> subscribeOnCbFromRealmObs() {
//
//        return mRootModel.fromDiskCb()
//                .map(cbRealm -> new Entry(cbRealm.getDateCurs(), cbRealm.getValue()))
//                .toList()
//                .doOnNext(entries -> {
//                    if (getView() != null) {
//                        getView().updateGraph(entries, RootActivity.CB_LABEL);
//                    }
//                });
//
//    }
//
//    private Observable<List<Entry>> subscribeOnForexFromRealmObs() {
//
//        return mRootModel.fromDiskForex()
//                .map(forexRealm -> new Entry(forexRealm.getDateCurs(), forexRealm.getValue()))
//                .toList()
//                .doOnNext(entries -> {
//                    if (getView() != null) {
//                        getView().updateGraph(entries, RootActivity.FOREX_LABEL);
//                    }
//                });
//    }

    //region ============= GreaphView ===============
    private Observable<List<DataPoint>> subscribeOnCbFromRealmObs() {

        return mRootModel.fromDiskCb()
                .map(cbRealm -> new DataPoint(new Date(cbRealm.getDateCurs()), cbRealm.getValue()))
                .toList()
                .doOnNext(dataPoints -> {
                    if (getView() != null) {
                        getView().updateGraph(dataPoints, GraphActivity.CB_LABEL);
                    }
                });

    }

    private Observable<List<DataPoint>> subscribeOnForexFromRealmObs() {

        return mRootModel.fromDiskForex()
                .map(forexRealm -> new DataPoint(new Date(forexRealm.getDateCurs()), forexRealm.getValue()))
                .toList()
                .doOnNext(dataPoints -> {
                    if (getView() != null) {
                        getView().updateGraph(dataPoints, GraphActivity.FOREX_LABEL);
                    }
                });

    }
    //endregion
    //endregion

}

package com.timofeev.dmdgraphapp.features.root;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.SecondScale;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.BaseSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.timofeev.dmdgraphapp.R;
import com.timofeev.dmdgraphapp.core.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.List;

public class GraphActivity extends BaseActivity implements IRootView, View.OnClickListener {


    public static final String CB_LABEL = "ЦБ";
    public static final String FOREX_LABEL = "Forex";
    private static final int CB_COLOR_GRAPH = Color.rgb(63, 81, 181);
    private static final int FOREX_COLOR_GRAPH = Color.rgb(255, 25, 6);
    private static final float CORRECT_RANGE = 0.2F;
    private RootPresenter mPresenter = RootPresenter.getInstance();
    private GraphView mGraphView;
    private TextView mUpdateTv;
    //    private LineGraphSeries<DataPoint> mBarCbSeries;
    private BarGraphSeries<DataPoint> mBarCbSeries;
    private LineGraphSeries<DataPoint> mLineForexSeries;
    private SecondScale mSecondScale;
    private Viewport mViewport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        mPresenter.bindView(this);
        $(R.id.update).setOnClickListener(this);
        mGraphView = $(R.id.line_graph);
        mUpdateTv = $(R.id.last_update);
        setupGraph();
    }

    @Override
    protected void onResume() {
        mPresenter.initView();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mPresenter.unbindView();
        super.onDestroy();
    }

    private void setupGraph() {

//        mBarCbSeries = createLineSeries(CB_LABEL, CB_COLOR_GRAPH);
        mBarCbSeries = new BarGraphSeries<>();
        mBarCbSeries.setTitle(CB_LABEL);
        mBarCbSeries.setSpacing(0);
        mBarCbSeries.setColor(CB_COLOR_GRAPH);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(CB_COLOR_GRAPH);

//        paint.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0));

        mBarCbSeries.setCustomPaint(paint);


        mLineForexSeries = createLineSeries(FOREX_LABEL, FOREX_COLOR_GRAPH);

        setupLegendOnGraph();
        setupSecondScale( mLineForexSeries);
        mGraphView.addSeries(mBarCbSeries);
        mViewport = mGraphView.getViewport();
        mGraphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this, new SimpleDateFormat("dd/MM")));
        mViewport.setXAxisBoundsManual(true);
        mGraphView.getGridLabelRenderer().setHorizontalLabelsAngle(90);
        mGraphView.getGridLabelRenderer().setVerticalLabelsVisible(false);
        mGraphView.getGridLabelRenderer().setHumanRounding(false);
        mGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);

    }

    @SafeVarargs
    private final void setupSecondScale(BaseSeries<DataPoint>... baseSeries) {
        mSecondScale = mGraphView.getSecondScale();

        for (BaseSeries<DataPoint> series : baseSeries) {
            mSecondScale.addSeries(series);
        }
    }

    private void setupLegendOnGraph() {
        mGraphView.getLegendRenderer().setVisible(true);
        mGraphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        mGraphView.getLegendRenderer().setSpacing(24);
        mGraphView.getLegendRenderer().setTextSize(24);
        mGraphView.getLegendRenderer().setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    @NonNull
    private LineGraphSeries<DataPoint> createLineSeries(String Label, int ColorGraph) {
        LineGraphSeries<DataPoint> lineGraphSeries = new LineGraphSeries<>();

        lineGraphSeries.setTitle(Label);
        lineGraphSeries.setColor(ColorGraph);

        lineGraphSeries.setDrawDataPoints(true);
        lineGraphSeries.setDataPointsRadius(8);
        lineGraphSeries.setThickness(4);

        return lineGraphSeries;
    }

    //region ============= IRootView ===============

    @Override
    public void updateGraph(List<DataPoint> dataPoints, String labelGraph) {

        if (labelGraph.equals(CB_LABEL)) {
            mBarCbSeries.resetData(transformListToArray(dataPoints));
        } else {
            mLineForexSeries.resetData(transformListToArray(dataPoints));
        }
        setMaxY(mLineForexSeries, mBarCbSeries);
        setMinY(mLineForexSeries, mBarCbSeries);
        setMaxX(mLineForexSeries, mBarCbSeries);
        setMinX(mLineForexSeries, mBarCbSeries);

    }

    private void setMinX(LineGraphSeries<DataPoint> lineForexSeries, BarGraphSeries<DataPoint> lineCbSeries) {
        if (lineForexSeries.getLowestValueX() > lineCbSeries.getLowestValueY()) {
            mViewport.setMinX(lineCbSeries.getLowestValueX());
        } else {
            mViewport.setMinX(lineForexSeries.getLowestValueX());
        }
    }

    private void setMaxX(LineGraphSeries<DataPoint> lineForexSeries, BarGraphSeries<DataPoint> lineCbSeries) {
        if (lineForexSeries.getHighestValueX() > lineCbSeries.getHighestValueX()) {
            mViewport.setMaxX(lineForexSeries.getHighestValueX());
        } else {
            mViewport.setMaxX(lineCbSeries.getHighestValueX());
        }
    }

    private void setMinY(LineGraphSeries<DataPoint> lineForexSeries, BarGraphSeries<DataPoint> lineCbSeries) {
        if (lineForexSeries.getLowestValueY() > lineCbSeries.getLowestValueY()) {
            mSecondScale.setMinY(lineCbSeries.getLowestValueY() - CORRECT_RANGE);
        } else {
            mSecondScale.setMinY(lineForexSeries.getLowestValueY() - CORRECT_RANGE);
        }
    }

    private void setMaxY(LineGraphSeries<DataPoint> lineForexSeries, BarGraphSeries<DataPoint> lineCbSeries) {

        if (lineForexSeries.getHighestValueY() > lineCbSeries.getHighestValueY()) {
            mSecondScale.setMaxY(lineForexSeries.getHighestValueY() + CORRECT_RANGE);
        } else {
            mSecondScale.setMaxY(lineCbSeries.getHighestValueY() + CORRECT_RANGE);
        }
    }

    private DataPoint[] transformListToArray(List<DataPoint> dataPoints) {
        DataPoint[] points = new DataPoint[dataPoints.size()];
        for (int i = 0; i < dataPoints.size(); i++) {
            points[i] = dataPoints.get(i);
        }
        return points;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLastUpdate(String date) {
        mUpdateTv.setText(String.format(getResources().getString(R.string.last_update), date));
    }
    //endregion

    //region ============= Events ===============
    @Override
    public void onClick(View view) {
        mPresenter.onClickUpdate();
    }
    //endregion
}

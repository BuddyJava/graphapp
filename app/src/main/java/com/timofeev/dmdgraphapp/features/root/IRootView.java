package com.timofeev.dmdgraphapp.features.root;

import com.jjoe64.graphview.series.DataPoint;
import com.timofeev.dmdgraphapp.core.IView;

import java.util.List;

public interface IRootView extends IView{


    void updateGraph(List<DataPoint> dataPoints, String labelGraph);
    void showMessage(String message);
    void showLastUpdate(String date);
}

package com.timofeev.dmdgraphapp.core;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.timofeev.dmdgraphapp.R;


@SuppressWarnings("unused")
public abstract class BaseActivity extends AppCompatActivity {
    @Nullable
    private ProgressDialog mProgressDialog;

    //region ============= ACTIVITY LOGIC ===============
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
    }

    @Nullable
    protected View getRootView() {
        ViewGroup rootGroup = (ViewGroup) this.findViewById(android.R.id.content);
        if (rootGroup == null) return null;
        return rootGroup.getChildAt(0);
    }
    //endregion

    //region ============= PROGRESS DIALOG ===============
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
            Window window = mProgressDialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.custom_dialog);
    }

    public void hideProgress() {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog.dismiss();
    }
    //endregion

    //region ============= MESSAGES ===============
//    public void showSnackbar(@StringRes int res) {
//        showSnackbar(getString(res));
//    }

//    public void showSnackbar(@Nullable String s) {
//        if (s == null || s.isEmpty()) return;
//        View rootView = getRootView();
//        if (rootView == null) return;
//
//        Snackbar snackbar = Snackbar.make(rootView, s, Snackbar.LENGTH_SHORT);
//
//        snackbar.show();
//    }

    //endregion


    //region ============= UTIL ===============
    public <T extends View> T $(@IdRes int id) {
        //noinspection unchecked
        return (T) findViewById(id);
    }

    public <V extends View, T extends View> T $(V view, @IdRes int id) {
        //noinspection unchecked
        return (T) view.findViewById(id);
    }

    public Context getContext() {
        return getApplicationContext();
    }



    //endregion

}


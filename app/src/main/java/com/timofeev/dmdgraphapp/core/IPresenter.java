package com.timofeev.dmdgraphapp.core;

import android.support.annotation.NonNull;

public interface IPresenter {

    void bindView(@NonNull IView view);
    void unbindView();
    IView getView();
}

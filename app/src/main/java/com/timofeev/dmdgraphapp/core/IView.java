package com.timofeev.dmdgraphapp.core;

public interface IView {

    void showProgress();
    void hideProgress();
}

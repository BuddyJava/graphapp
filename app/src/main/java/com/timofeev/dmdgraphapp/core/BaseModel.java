package com.timofeev.dmdgraphapp.core;

import com.timofeev.dmdgraphapp.data.managers.DataManager;
import com.timofeev.dmdgraphapp.di.component.DaggerModelComponent;
import javax.inject.Inject;

public class BaseModel {

    @Inject
    protected DataManager mDataManager;


    public BaseModel() {
        DaggerModelComponent.create().inject(this);
    }
}

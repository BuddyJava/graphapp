package com.timofeev.dmdgraphapp.core;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class BasePresenter<V extends IView> implements IPresenter {

    @Nullable
    private V view;

    @Override
    public void bindView(@NonNull IView view) {
        this.view = (V) view;
    }

    @Override
    public void unbindView() {
        if(view != null){
            this.view = null;
        }else {
            throw new IllegalArgumentException("you don't bindView");
        }
    }

    @Override
    public V getView() {
        return view;
    }
}

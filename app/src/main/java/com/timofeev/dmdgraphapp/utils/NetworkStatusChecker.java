package com.timofeev.dmdgraphapp.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.timofeev.dmdgraphapp.App;

import rx.Observable;

public class NetworkStatusChecker {

    public static Observable<Boolean> isInternetAvailable(){
        return Observable.just(isNetworkConnect());
    }

    @NonNull
    private static boolean isNetworkConnect() {
        ConnectivityManager manager = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}

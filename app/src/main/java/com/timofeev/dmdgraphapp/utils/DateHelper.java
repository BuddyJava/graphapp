package com.timofeev.dmdgraphapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

    public static final String CB_FORMAT = "dd.MM.yyyy";
    public static final String FOREX_FORMAT = "yyyy-MM-dd";
    public static final String SEND_CB_FORMAT = "dd/MM/yyyy";
    public static final String SEND_FX_FORMAT = "yyyy-MM-dd";
    public static final String TEXT_UPDATE_FORMAT = "dd.MM.yyyy - HH.mm";
    public static final long RANGE_CURS = 1000*60*60*24*7;

    public static Date parseDateFromString(String source, String patternFormat) {

        Date date = null;

        try {
            SimpleDateFormat format = new SimpleDateFormat(patternFormat);
            date = format.parse(source);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) {
            return new Date();
        } else {
            return date;
        }

    }

    public static String parseStringFromDate(Date source,String pattern) {
        String str;

        SimpleDateFormat format = new SimpleDateFormat(pattern);
        str = format.format(source);

        return str;
    }

    public static String formatDateForTextView(Date date){

        return new SimpleDateFormat(TEXT_UPDATE_FORMAT).format(date);
    }
}

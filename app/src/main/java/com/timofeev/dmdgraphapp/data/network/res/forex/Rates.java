package com.timofeev.dmdgraphapp.data.network.res.forex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Rates{

	@SerializedName("RUB")
	@Expose
	private double mRUB;

	public void setRUB(double rUB){
		this.mRUB = rUB;
	}

	public double getRUB(){
		return mRUB;
	}

	@Override
 	public String toString(){
		return 
			"Rates{" + 
			"mRUB = '" + mRUB + '\'' +
			"}";
		}
}
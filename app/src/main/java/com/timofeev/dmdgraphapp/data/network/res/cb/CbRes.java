package com.timofeev.dmdgraphapp.data.network.res.cb;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ValCurs")
public class CbRes {

    @Attribute
    private String DateRange1;
    @Attribute
    private String DateRange2;

    @ElementList(inline = true)
    private List<Record> Record;
    @Attribute
    private String name;
    @Attribute
    private String ID;

    public String getDateRange1() {
        return DateRange1;
    }

    public void setDateRange1(String DateRange1) {
        this.DateRange1 = DateRange1;
    }

    public String getDateRange2() {
        return DateRange2;
    }

    public void setDateRange2(String DateRange2) {
        this.DateRange2 = DateRange2;
    }


    public List<Record> getRecord() {
        return Record;
    }

    public void setRecord(List<Record> Record) {
        this.Record = Record;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "CbRes [DateRange1 = " + DateRange1 + ", DateRange2 = " + DateRange2 + ", Record = " + Record + ", name = " + name + ", ID = " + ID + "]";
    }

}

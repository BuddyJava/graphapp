package com.timofeev.dmdgraphapp.data.storage;


import com.timofeev.dmdgraphapp.data.network.res.cb.Record;
import com.timofeev.dmdgraphapp.utils.DateHelper;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CbRealm extends RealmObject{

    @PrimaryKey
    private long dateCurs;
    private String idVal;
    private float value;


    public CbRealm() {
    }


    public CbRealm(Record record) {
        this.dateCurs = DateHelper.parseDateFromString(record.getDate(), DateHelper.CB_FORMAT).getTime();
        this.idVal = record.getId();
        this.value = Float.valueOf(record.getValue().replace(',', '.'));

    }

    public long getDateCurs() {
        return dateCurs;
    }

    public String getIdVal() {
        return idVal;
    }

    public float getValue() {
        return value;
    }

}

package com.timofeev.dmdgraphapp.data.storage;

import com.timofeev.dmdgraphapp.data.network.res.forex.ForexRes;
import com.timofeev.dmdgraphapp.utils.DateHelper;
import com.timofeev.dmdgraphapp.utils.L;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ForexRealm extends RealmObject {

    @PrimaryKey
    private long dateCurs;
    private float value;

    public ForexRealm() {
    }

    public ForexRealm(ForexRes forexRes, Date date) {

        this.dateCurs = date.getTime();
        this.value = (float) forexRes.getRates().getRUB();
    }

    public long getDateCurs() {
        return dateCurs;
    }

    public float getValue() {
        return value;
    }

}

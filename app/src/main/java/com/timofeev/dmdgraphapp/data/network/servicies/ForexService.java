package com.timofeev.dmdgraphapp.data.network.servicies;

import com.timofeev.dmdgraphapp.data.network.res.forex.ForexRes;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ForexService {


    String SYMBOL = "RUB";

    @GET("latest")
    Observable<Response<ForexRes>> getForexValCurs(@Query("symbols") String symbol, @Query("date") String date);

}

package com.timofeev.dmdgraphapp.data.managers;

import com.timofeev.dmdgraphapp.data.network.res.cb.CbRes;
import com.timofeev.dmdgraphapp.data.network.res.cb.Record;
import com.timofeev.dmdgraphapp.data.network.res.forex.ForexRes;
import com.timofeev.dmdgraphapp.data.storage.CbRealm;
import com.timofeev.dmdgraphapp.data.storage.ForexRealm;
import com.timofeev.dmdgraphapp.data.storage.LastUpdateRealm;
import com.timofeev.dmdgraphapp.utils.L;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;
import rx.schedulers.Schedulers;


public class RealmService {

    private Realm mReamInstance;

    public void saveCbResToRealm(List<Record> records) {
        Realm realm = Realm.getDefaultInstance();

        if (!records.isEmpty()) {
//            deleteFromRealm(CbRealm.class);
            Observable.from(records)
                    .map(CbRealm::new)
                    .toList()
                    .subscribe(cbRealms -> realm.executeTransaction(realm1 -> realm1.insertOrUpdate(cbRealms)));
        }
        realm.close();
    }


    public void saveForexResToRealm(ForexRes forexRes, Date date) {
        Realm realm = Realm.getDefaultInstance();

        Observable<ForexRes> forexResObs = Observable.just(forexRes);
        Observable<Date> dateObs = Observable.just(date);

        Observable.combineLatest(forexResObs, dateObs, ForexRealm::new)
                .subscribe(forexRealm -> realm.executeTransaction(realm1 -> realm1.insertOrUpdate(forexRealm)));

        realm.close();
    }

    public void deleteFromRealm(Class<? extends RealmObject> entity) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.delete(entity));
        realm.close();
    }

    public void saveLastUpdate(Date date) {
        Realm realm = Realm.getDefaultInstance();

        Observable.just(date)
                .map(date1 -> new LastUpdateRealm(0, date1))
                .subscribe(lastUpdateRealm -> realm.executeTransaction(realm1 -> realm1.insertOrUpdate(lastUpdateRealm)));

        realm.close();
    }


    public Observable<CbRealm> loadCbRealmFromRealm() {

        Realm realm = Realm.getDefaultInstance();

        RealmResults<CbRealm> results = getQueryRealmInstance().where(CbRealm.class).findAll();
        L.e("getCbRealmFromRealm");

        Observable<CbRealm> cbRealmObs = results
                .sort("dateCurs")
                .asObservable()
                .first()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);


        realm.close();
        return cbRealmObs;
    }

    public Observable<ForexRealm> loadForexRealmFromRealm() {

        Realm realm = Realm.getDefaultInstance();

        RealmResults<ForexRealm> results = realm.where(ForexRealm.class).findAll();
        Observable<ForexRealm> forexRealmObs = results
                .sort("dateCurs")
                .asObservable()
                .first()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
        L.e("getCbRealmFromRealm");

        realm.close();
        return forexRealmObs;
    }

    private Realm getQueryRealmInstance() {
        if (mReamInstance == null || mReamInstance.isClosed()) {
            mReamInstance = Realm.getDefaultInstance();
        }
        return mReamInstance;
    }

    public Observable<LastUpdateRealm> loadLastUpdate() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<LastUpdateRealm> updateResults = realm.where(LastUpdateRealm.class).findAll();
        realm.close();
        return updateResults.asObservable()
                .first()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }
}

package com.timofeev.dmdgraphapp.data.storage;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class LastUpdateRealm extends RealmObject {

    @PrimaryKey
    private int id;
    private Date lastUpdate;

    public LastUpdateRealm() {
    }

    public LastUpdateRealm(int id, Date lastUpdate) {
        this.id = id;
        this.lastUpdate = lastUpdate;
    }

    public int getId() {
        return id;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
}

package com.timofeev.dmdgraphapp.data.managers;


import com.timofeev.dmdgraphapp.data.network.ResponseCallTransformer;
import com.timofeev.dmdgraphapp.data.network.res.cb.CbRes;
import com.timofeev.dmdgraphapp.data.network.res.cb.Record;
import com.timofeev.dmdgraphapp.data.network.res.forex.ForexRes;
import com.timofeev.dmdgraphapp.data.network.servicies.CbService;
import com.timofeev.dmdgraphapp.data.network.servicies.ForexService;
import com.timofeev.dmdgraphapp.data.storage.CbRealm;
import com.timofeev.dmdgraphapp.data.storage.ForexRealm;
import com.timofeev.dmdgraphapp.data.storage.LastUpdateRealm;
import com.timofeev.dmdgraphapp.di.Injector;
import com.timofeev.dmdgraphapp.di.module.NetworkModule;
import com.timofeev.dmdgraphapp.utils.DateHelper;
import com.timofeev.dmdgraphapp.utils.L;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.RegEx;
import javax.inject.Inject;

import rx.Observable;
import rx.schedulers.Schedulers;

import static com.timofeev.dmdgraphapp.utils.DateHelper.CB_FORMAT;
import static com.timofeev.dmdgraphapp.utils.DateHelper.FOREX_FORMAT;
import static com.timofeev.dmdgraphapp.utils.DateHelper.SEND_CB_FORMAT;
import static com.timofeev.dmdgraphapp.utils.DateHelper.SEND_FX_FORMAT;
import static com.timofeev.dmdgraphapp.utils.DateHelper.parseDateFromString;
import static com.timofeev.dmdgraphapp.utils.DateHelper.parseStringFromDate;

public class DataManager {

    private static final String TAG = "DataManager";
    //region ============= INJECT ===============
    @Inject
    CbService mCbService;
    @Inject
    ForexService mForexService;
    @Inject
    RealmService mRealmService;
    //endregion

    public static DataManager INSTANCE = null;

    private DataManager() {
        Injector.getAppComponent()
                .dataComponent()
                .networkModule(new NetworkModule())
                .build()
                .inject(this);
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {

            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    //region ============= NETWORK ===============
    public Observable<CbRes> getCbResFromNetwork(Date date) {
        return mCbService.getCbValCurs(parseStringFromDate(new Date(date.getTime() - DateHelper.RANGE_CURS), SEND_CB_FORMAT),
                parseStringFromDate(date, SEND_CB_FORMAT),
                CbService.EUR_ID)
                .compose(new ResponseCallTransformer<>())
                .subscribeOn(Schedulers.io())
                .map(this::refactResponseCb)
//                .observeOn(Schedulers.computation())
                .doOnNext(records -> mRealmService.saveCbResToRealm(records))
                .flatMap(cbRes -> Observable.empty());
    }

    private List<Record> refactResponseCb(CbRes cbRes) {
        List<Record> newListRec = cbRes.getRecord();
        long prevDate = parseDateFromString(newListRec.get(0).getDate(), CB_FORMAT).getTime();
        Record lostRecord = newListRec.get(0);


        for (Record record : cbRes.getRecord()) {
            if (parseDateFromString(record.getDate(), CB_FORMAT).getTime() != prevDate + 1000 * 60 * 60 * 24 && record != newListRec.get(0)) {
                for (int i = 1; i <= 2; i++) {

                    newListRec.add(new Record(lostRecord.getValue(),
                            parseStringFromDate(new Date(prevDate + 1000 * 60 * 60 * 24 * i), CB_FORMAT)));
                }
                break;
            } else {
                lostRecord = record;
                prevDate = parseDateFromString(lostRecord.getDate(), CB_FORMAT).getTime();
            }
        }
        for (Record record : newListRec) {
            L.e(record.toString());
        }
        return newListRec;
//        return Observable.from(cbRes.getRecord())
//                .doOnNext(record -> {
//                    if(parseDateFromString(record.getDate(), CB_FORMAT).getTime() != date + 1000*60*60*24 ){
//
//                    }else {
//                         = parseDateFromString(record.getDate(), CB_FORMAT).getTime();
//                    }
//                }).toList();


    }

    public Observable<ForexRes> getForexCursFromNetwork(Date date) {
        return Observable.range(0, 7)
                .map(integer -> new Date(date.getTime() - (1000 * 60 * 60 * 24 * integer)))
                .flatMap(this::requestAtFrorex)
                .flatMap(forexRes -> Observable.empty());
    }

    private Observable<ForexRes> requestAtFrorex(Date date) {

        String dateStr = parseStringFromDate(date, SEND_FX_FORMAT);

        return mForexService.getForexValCurs(ForexService.SYMBOL, dateStr)
                .compose(new ResponseCallTransformer<>())
                .doOnNext(forexRes -> mRealmService.saveForexResToRealm(forexRes, parseDateFromString(dateStr, FOREX_FORMAT)))
                .subscribeOn(Schedulers.io());

    }
    //endregion


    //region ============= REALM ===============
    public Observable<CbRealm> getCbRealmFromRealm() {
        return mRealmService.loadCbRealmFromRealm();
    }

    public Observable<ForexRealm> getForexRealmFromRealm() {
        return mRealmService.loadForexRealmFromRealm();
    }

    public Observable<LastUpdateRealm> getLastUpdateFromRealm() {
        return mRealmService.loadLastUpdate();
    }

    public void saveLastUpdate(Date date) {
        mRealmService.saveLastUpdate(date);
    }
    //endregion

}

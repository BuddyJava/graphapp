package com.timofeev.dmdgraphapp.data.network.res.cb;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;

@Root(name = "Record")
public class Record {

    @Element
    private String Value;
    @Attribute
    private String Date;
    @Element
    private String Nominal;
    @Attribute
    private String Id;

    public String getValue() {
        return Value;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getNominal() {
        return Nominal;
    }

    public void setNominal(String Nominal) {
        this.Nominal = Nominal;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    @Override
    public String toString() {
        return "Record [Value = " + Value + ", Date = " + Date + ", Nominal = " + Nominal + ", Id = " + Id + "]";
    }

    public Record() {
    }

    public Record(String value, String date) {
        Value = value;
        Date = date;
        Nominal = "1";
        Id = "R01239";
    }
}

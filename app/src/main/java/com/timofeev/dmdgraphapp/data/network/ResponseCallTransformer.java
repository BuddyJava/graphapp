package com.timofeev.dmdgraphapp.data.network;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.timofeev.dmdgraphapp.data.network.error.NetworkAvailableError;
import com.timofeev.dmdgraphapp.utils.L;
import com.timofeev.dmdgraphapp.utils.NetworkStatusChecker;

import retrofit2.Response;
import rx.Observable;

public class ResponseCallTransformer<R> implements Observable.Transformer<Response<R>, R> {


    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                            return Observable.just(rResponse.body());
                        case 404:
                            return Observable.error(new Throwable("Страница не найдена"));
                        case 500:
                            return Observable.error(new Throwable("Сервер не отвечает"));
                        default:
//                            return Observable.error(ErrorUtils.parseError(rResponse));
                            return Observable.error(new Throwable("Не удалось получить данные"));
                    }
                });
    }
}

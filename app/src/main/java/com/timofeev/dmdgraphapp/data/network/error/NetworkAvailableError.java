package com.timofeev.dmdgraphapp.data.network.error;


public class NetworkAvailableError extends Throwable {
    public NetworkAvailableError() {
        super("Соединение отсутсвует");
    }
}

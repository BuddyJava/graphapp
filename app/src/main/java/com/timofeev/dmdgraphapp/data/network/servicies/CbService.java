package com.timofeev.dmdgraphapp.data.network.servicies;

import com.timofeev.dmdgraphapp.data.network.res.cb.CbRes;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface CbService {

    String EUR_ID = "R01239";

    @GET("XML_dynamic.asp")
    Observable<Response<CbRes>> getCbValCurs(@Query("date_req1") String fromDate,
                                                   @Query("date_req2") String toDate,
                                                   @Query("VAL_NM_RQ") String valId);
}
